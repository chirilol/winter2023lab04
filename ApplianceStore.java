import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Toaster[] toaster = new Toaster[2]; //shorter than the original to test quickly
		Scanner keyboard = new Scanner(System.in); 
		for(int i = 0; i<toaster.length; i++){
			toaster[i] = new Toaster("good", 99.99, 2);
			System.out.println("The brand: "+toaster[i].getBrand());
			System.out.println("The quality: "+toaster[i].getQuality());
			System.out.println("The cost: "+toaster[i].getCost());
			System.out.println("The bread: "+toaster[i].getCost());
		}
		System.out.println("(After Loop) What is the quality?");
		toaster[toaster.length-1].setQuality(keyboard.next());
		System.out.println("(After Loop) What is the cost?");
		toaster[toaster.length-1].setCost(keyboard.nextDouble());
		System.out.println("(After Loop) What is the amount of bread?");
		toaster[toaster.length-1].setBread(keyboard.nextInt());
		
		System.out.println("The brand: "+toaster[toaster.length-1].getBrand());
		System.out.println("The quality: "+toaster[toaster.length-1].getQuality());
		System.out.println("The cost: "+toaster[toaster.length-1].getCost());
		System.out.println("The bread: "+toaster[toaster.length-1].getBread());
		
		toaster[1].grill();
		toaster[0].info();
	}
	
}
import java.util.Random;
public class Toaster{
	
	//Fields
	
	private String brand;
	private String quality;
	private double cost;
	private int breadAmount; //new field
	
	//Constructor
	public Toaster(String quality, double cost, int bread){
		this.brand = "SAMSUNG";
		this.quality = quality;
		this.cost = cost;
		this.breadAmount = bread; //new field
	}
	
	//Getter
	public String getBrand(){
		return this.brand;
	}
	public String getQuality(){
		return this.quality;
	}
	public double getCost(){
		return this.cost;
	}
	public int getBread(){
		return this.breadAmount;
	}
	
	//Setter
	
	public void setQuality(String newQuality){
		if(newQuality!=null){
			this.quality = newQuality;
		}
	}
	public void setCost(double newCost){
		if(newCost>0){
			this.cost = newCost;
		}
	}
	public void setBread(int newBread){
		if(newBread>0){
			this.breadAmount = newBread;
		}
	}
	
	//Methods
	public void grill(){
		Random random = new Random();
		int decision = random.nextInt(2);
		if(decision==0){
			System.out.println("Your toaster is broken. Try again. :(");
		}else{
			System.out.println("Your toaster is working. :)");
		}
		grillCalculator(decision);
	}
	private void grillCalculator(int decision){ 
		if(decision==1){
			System.out.println("You have grilled "+this.breadAmount+" loaf(s) of bread.");
		}
	}
	public void info(){
		System.out.println("The "+this.brand+" product offers a "+this.quality+" quality toaster that costs only "+this.cost+"$!");
	}

}